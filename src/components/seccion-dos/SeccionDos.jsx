import React from 'react'

class SeccionDos extends React.Component {

  render() {
    return (
      <main className="container d-flex h-100 align-items-center justify-content-center">
        <div className="row">
          <div className="col-12">
            <h1>Seccion 2</h1>
            <article>
              <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Soluta mollitia veniam praesentium libero officia? Iusto, alias ipsa. Doloribus ipsum illum cumque quae veniam alias minus nobis aspernatur, dolorem voluptas voluptatibus?</p>
            </article>
          </div>
        </div>
      </main>
    );
  }

}

export default SeccionDos;