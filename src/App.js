import React from 'react';
import { BrowserRouter as Router, Switch } from "react-router-dom";

// Estilos
import './assets/scss/styles.scss';

// Componentes
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import Inicio from './components/inicio/Inicio';
import SeccionUno from './components/seccion-uno/SeccionUno';
import SeccionDos from './components/seccion-dos/SeccionDos';

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Switch>
          <Router path="/" exact>
            <Inicio />
          </Router>
          <Router path="/seccion-1">
            <SeccionUno />
          </Router>
          <Router path="/seccion-2">
            <SeccionDos />
          </Router>
        </Switch>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
